var express = require('express');
var path = require('path');
var app = express();


app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/html/home.html'));
});

app.use(express.static(__dirname + '/html'));

app.listen(8080, function() {
    console.log('Listening at port 8080');
});