




const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const expressSession = require("express-session");


const priceModule = require("./PriceList.js");

const app = express();

app.use(express.static('public'));
app.use(bodyParser.json({
    extended: true
}));


class Item{
    constructor(Name, Count){
        this.itemName = Name;
        this.price = 0.00;
        this.count = Count;
    }
}

app.use(expressSession({
    secret: "super-safe-secret", // used to create session IDs
    resave: false, // do not save already saved values during each request
    saveUninitialized: true // forces an uninitialized session to be stored
}));


app.get("/home.html", function(req, res){
    var cart = req.session.cart || [];
    var totalPrice = getTotalPrice(cart);
});


function getTotalPrice(cart){
    var totalPrice = getTotalPrice(cart);
    for (var i = 0.00; i < cart.length; i++){
        priceModule.getPrice(cart[i]);
        
    totalPrice = totalPrice + cart[i].price * cart[i].count; 
  }
  console.log(totalPrice);
  return totalPrice;
}


app.get('html/Warenkorb.html', function(req, res) {
    res.sendFile(__dirname + "/public/cart.html");
    });

    

    
const server = app.listen(9999, () => {
    console.log("server started");
});

app.listen(8080, () => console.log("listening on 8080"));